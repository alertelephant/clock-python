'''
Created on 19 Mar 2021

@author: philip
'''
import json


def fix_news() -> None:
    """
    Fix corrupted news schedules caused by API fiddling!
    """
    
    news_time_segments = []
    
    for hour in range(7, 23):
        news_time_segments.append(
            [[hour, 30], [hour, 35]]                    
        )
    
    settings_file = open("settings.json", "r")
    
    settings = json.load(settings_file)
    
    settings_file.close()
    
    settings["news_time_segments"] = news_time_segments
    
    settings_file = open("settings.json", "w")
    
    json.dump(settings, settings_file)
    
    settings_file.close()

if __name__ == "__main__":
    fix_news()
