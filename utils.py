'''
Created on 11 Feb 2021

Utilities for use with the clock system.

@author: philip
'''
from typing import Union
from asyncio import sleep as async_sleep
from enum import Enum
import signal
from subprocess import run
import busio
import board
import bme680
import adafruit_veml7700
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from rgbmatrix.graphics import Font, DrawText, Color
from datetime import time
from os.path import dirname
from os import getpid
from pathlib import Path
from json import load, JSONDecodeError, dump
from asyncio.tasks import sleep


class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Also, the decorated class cannot be
    inherited from. Other than that, there are no restrictions that apply
    to the decorated class.

    To get the singleton instance, use the `instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


@Singleton
class ClockSettings:
    """
    This clock's settings, this is persisted to disk so on re-load it is picked up.
    This defaults to sensible defaults if no settings file exits.
    """
    
    # The minimum brightness for "dusk/darkness"
    dark_brightness = 0.10
    # LUX range, this sets the range from 0 to the value of LUX that 
    # is deemed to represent "daylight" (brightness level = 1.0).
    # Defaults to a LUX of 3.5 for "daylight", this value is low due to
    # the sensor having to be mounted, on the back of the clock.
    lux_range = 3.5
    
    # Current brightness.
    brightness = 1.0
    
    # The RGB color for the time text.
    time_color = Color(252,165,3)
    # The RGB color for the environment text (temperature/pressure e.t.c.)
    environment_color = Color(94,92,247)
    # The RGB color for the news feed text.
    news_color = Color(150,232,242)
    
    # Set news to show allways - this is mostly used in co-junction with news_reaload for signalling changing the
    # news color and marquee settings.
    news_always = False
    
    # Disable news all-together.
    news_never = False
    
    # Signals a news reload, which will reset the news. 
    # This is used by external actors to signal that news settings have changed and a
    # reload of the news is required.
    # This is meaningless if news_always is false.
    news_reload = False
    
    # Set of time ranges to display the news, as time deltas
    news_time_segments = []
    
    # If set True this switches to getting environment data, from the GDS system.
    environment_outside = False
    
    # The the delay in seconds before updating the marquee.
    marquee_scroll_delay = 1.0
    # The distance to scroll the marquee, for each step
    marquee_scroll_distance = 4
    
    
    def __init__(self):
        """
        Load in settings if they exist.
        """
        
        self.load_settings()
        # If news_time_segments is not set we generate time intervals of 1/2 hour between 
        # 07:30 and 22:30, each one lasting 5 minutes.  
        if not self.news_time_segments:
            for hour in range(7, 23):
                self.news_time_segments.append(
                        [time(hour=hour, minute=30), time(hour=hour, minute=35)]                    
                    )
        
        self.generate_settings()
                    
    def load_settings(self):
        """
        Load in the settings.
        """
        
        path = Path(dirname(__file__)) / "settings.json"
        
        if path.exists():            
            with path.open('r') as settings_file:
                try:
                    data = load(settings_file)
                    for key, value in data.items():
                        
                        if "_color" in key:
                            # A color setting consists of a list of three value denoting R,G,B
                            setattr(self, key, Color(value[0], value[1], value[2]))
                            continue
                        if key == "news_time_segments":
                            # The value will be a list of tuples of time intervals, consisting of start time 
                            # and end time.
                            # Seconds are always zero.
                            # Format is:
                            # [
                            #   [ [hour_start, minute_start], [hour_end, minute_end]], ... 
                            # ]
                            # Convert start + end to time objects.
                            self.news_time_segments = [
                                    [ time(hour=time_interval[0][0], minute=time_interval[0][1]), 
                                      time(hour=time_interval[1][0], minute=time_interval[1][1]) ]
                                for time_interval in value]
                            continue
                        
                        # Everything else maps directly to properties
                        setattr(self, key, value)
                        
                    
                except JSONDecodeError as ex:
                    # Log issue to console - we default to defaults and re-generate the json file.
                    print(f"ClockSettings JSON error: {ex}")
                    self.generate_settings()
    
    def generate_settings(self):
        """
        This is used to generate the settings file for use by external actors, to 
        change settings.
        """
        
        path = Path(dirname(__file__)) / "settings.json"
#         
        if path.exists():
            # Settings already generated.
            return
         
        # Convert the news time intervals back into hour/minute tuples for serialisation
        news_time_segments = [
            [[start.hour, start.minute],[end.hour, end.minute]] for start,end in self.news_time_segments            
            ]
        
        # Gather up the settings for serialisation
        settings = {
            "news_time_segments": news_time_segments,
            "time_color": [self.time_color.red, self.time_color.green, self.time_color.blue],
            "environment_color": [self.environment_color.red, self.environment_color.green, self.environment_color.blue],
            "news_color": [self.news_color.red, self.news_color.green, self.news_color.blue],
            "news_always": self.news_always,
            "news_never": self.news_never,
            "environment_outside": self.environment_outside,
            "marquee_scroll_delay": self.marquee_scroll_delay,
            "marquee_scroll_distance": self.marquee_scroll_distance,
            "brightness": self.brightness,
            "dark_brightness": self.dark_brightness,
            "lux_range": self.lux_range
            }
        
        with path.open("w") as settings_file:
            dump(settings, settings_file)
        
        
@Singleton        
class ClockState:
    """
    Holds the state and initialisation of i2c devices.
    IMPORTANT! This must be initialised before the Matrix is initialised, or bad things will happen!
    """
    # It is a known issue with the ADAFruit RGB matrix HAT and I2C bus where they don't "play well" together,
    # if the matrix is initialised before i2c.
    
    
    i2c_environment_sensor: busio.I2C = None
    i2c_light_sensor: adafruit_veml7700.VEML7700 = None
    
    # If set True the environment service will "pause" and not write data to the matrix.
    # This allows other services to use the "real estate" that the environment service used.
    environment_paused = False
    
    # News will be gathered and shown when this is True
    news_active = False
    
    # If set True this signals all asyncs services to quit.
    terminate = False
    
    # Last read "clock" temperature, IE temperature in the room the clock is in.
    temperature_clock = 0.0
    # Last read "clock" pressure, IE the pressure in the room the clock is in.
    pressure_clock = 0.0
    # Last read humidity, IE the humidity in the room the clock is in
    humidity_clock = 0.0
    
    # Duplicate of above but with data obtained from the sensors, in the GSD system.
    temperature_external = 0.0
    pressure_external = 0.0
    humidity_external = 0.0
    
    # This process PID
    pid = getpid()
    
    def __init__(self) -> None:
        """
        Initialise the I2C multiplexor and sensors.
        """
        
        # We have now switched to using circuit Python for I2C access.
        i2c = busio.I2C(board.SCL, board.SDA) 
        
        # We lock the i2c bus so we can setup the multiplexor
        if i2c.try_lock():
            
            # Enable channels for the light and environment sensors, connected to the multiplexor.
            # Environment sensor is on channel 0 and the light sensor is on channel 1
            # The multiplexor is a TCA9548A on default address 0x70, it has no command per say, so the first
            # "command" byte is the channel selector value.
            # For our use we have requested channel 0 and 1 (0b11) is switched through, both sensors are on different 
            # addresses, so we can have both channels active, without conflict.
            i2c.writeto(0x70, bytes([0b11]))
        else:
            # We error, we should have been able to lock the i2c bus, this means something else is 
            # trying to access it!
            raise RuntimeError("Unable to get a lock on the i2c bus. Some other process is accessing the i2c!")
        
        # The individual libraries can handle the locking themselves.
        i2c.unlock()
        
        # Setup the individual drivers for each sensor.
        self.i2c_environment_sensor = bme680.BME680(0x77)
        self.i2c_light_sensor = adafruit_veml7700.VEML7700(i2c)
    
    def commit_state(self):
        """
        Commit the current state to a state JSON file, for external actors to make use of. 
        """
        
        path = Path(dirname(__file__)) / "state.json"
        state = {
            "temperature_clock": self.temperature_clock,
            "pressure_clock": self.pressure_clock,
            "humidity_clock": self.humidity_clock,
            "temperature_external": self.temperature_external,
            "pressure_external": self.pressure_external,
            "humidity_external": self.humidity_external,
            "pid": self.pid
            }
        
        with path.open('w') as state_file:
            dump(state, state_file)


# Initialise clock settings for convenience.
clock_settings = ClockSettings.instance()

@Singleton
class Matrix:
    """
    The matrix display controller that wraps the matrix libraries and provides a means to display text,
    on the matrix.
    IMPORTANT: This has to be initialised AFTER ClockState
    """
    
    matrix: RGBMatrix = None
    canvas = None
    
    ####
    # Stores the text data and meta data that is to be rendered out.
    # Contains text objects of the form:
    # {
    # "<ident>": {
    #    "text": "<text to display>",
    #    "font": <Font object for font to render text in>,
    #    "color": <Color object representing the text color - this will be adjusted for brightness weighting>,
    #    "verticle_position": <Vertical pixel position>,
    #    "horizontal_position": <Horisontal pixel position>,
    #    To display text as a scrolling marquee set this key to the direction of scrolling.
    #    "marquee_direction": MarqueeDirection.LTR | MarqueeDirection.RTL
    #
    # }, ... }
    #### 
    text_data = {}
    update = False
    font_cache = {}
    
    class MarqueeDirection(Enum):
        LTR = 1
        RTL = 2
    
    
    def __init__(self):
        """
        Initialise the matrix and a canvas to allow writing to it.
        """
        
        matrix_options = RGBMatrixOptions()
        matrix_options.rows = 32
        matrix_options.cols = 64
        matrix_options.chain_length = 1
        matrix_options.parallel = 1
        # matrix_options.hardware_mapping = 'adafruit-hat'
        # Switched to PWM mode (bridged pin 4 and 18 together on the HAT, via removable link), in 
        # "normal" mode the display flickered badly, when the brightness was reduced.
        matrix_options.hardware_mapping = 'adafruit-hat-pwm'
        self.matrix = RGBMatrix(options = matrix_options)
        self.canvas = self.matrix.CreateFrameCanvas()
    
    async def service(self) -> None:
        """
        The matrix render service, this is run as a service asynchronously and updates the display every so often.
        """
        
        # Dereference speed up.
        text_data = self.text_data
        clock_state = ClockState.instance()
        clock_settings = ClockSettings.instance()
#         update = self.update
        force_update = False
        h_pos = 0
        canvas = self.canvas
        
        print("Matrix render service running.")
        while not clock_state.terminate:
            
            if not self.update:
                await async_sleep(2)
                continue
            
            has_marquee = False
            brightness = clock_settings.brightness
            marquee_scroll_distance = clock_settings.marquee_scroll_distance
            
            # Clear the canvas out for re-drawing
            canvas.Clear()
            
            for key, text_obj in text_data.items():
                # Color brightness adjustment - we have to operate on a copy of the original color object,
                # since it holds a reference.
                color = text_obj["color"]
                color = Color(
                        color.red,
                        color.green,
                        color.blue
                    )
                # Brightness adjustment - this is done in the loop since it can now be dynamically 
                # updated.
                # Also this is done this way since the driver does not support brightness adjustment, in
                # its own right.
                color.red *= brightness
                color.green *= brightness
                color.blue *= brightness
                
                # The matrix driver does not take well to font loading, so we cache any already loaded 
                # fonts, and re-use them.
                font_path = text_obj["font"]
                if font_path in self.font_cache:
                    font = self.font_cache[font_path]
                else:
                    font = Font()
                    font.LoadFont(font_path)
                    self.font_cache[font_path] = font
                
                marquee = text_obj.get("marquee", None)
                
                if marquee is not None:
                    # We need continuous updating
                    has_marquee = True                    
                    
                    h_pos = marquee.get("pixel_position", 
                                        -canvas.width if marquee["direction"] == self.MarqueeDirection.LTR else canvas.width)
                else:
                    h_pos = text_obj["horizontal_position"]
                
                # Render the text to the canvas and get a size for marquee re-positioning.
                m_size = DrawText(canvas, font, h_pos, 
                             text_obj["vertical_position"],
                             color,
                             text_obj["text"])
                                
                if marquee is not None:
                    
                    # Update marquee position
                    h_pos += -marquee_scroll_distance if marquee["direction"] == self.MarqueeDirection.RTL else marquee_scroll_distance
                    
                    if (h_pos + m_size) < 0 and marquee["direction"] == self.MarqueeDirection.RTL:
                        h_pos = canvas.width
                    
                    if h_pos > canvas.width and marquee["direction"] == self.MarqueeDirection.LTR:
                        h_pos = -canvas.width
                    
                    # Update the position in the parent object.
                    self.text_data[key]["marquee"]["pixel_position"] = h_pos
                    # Sleep for a designated time
                    await sleep(clock_settings.marquee_scroll_delay)
            
            # Force an update again if we have a marquee.
            self.update = has_marquee
                    
            # Write canvas to matrix
            self.matrix.SwapOnVSync(canvas)
                    
        print("Matrix render service terminated.")
            
    
    def text(self, text: str, font:str, color:Color, vertical_position:int=0, horizontal_position:int=0, 
             marquee_direction: Union[MarqueeDirection,None]=None) -> None:
        """
        Add text to the text structure for later rendering on the matrix.
        Text at the same vertical and horizontal positions, will be overwritten.
        """
        
        ident = f"{vertical_position} || {horizontal_position}"
        
        # Separately compiled to do equality testing. 
        data = {
                "text": text,
                "font": font,
                "color": color,
                "vertical_position": vertical_position,
                "horizontal_position": horizontal_position            
            }
        
        if marquee_direction is not None:
            data["marquee"] = {
                    "direction": marquee_direction
                    }
        
        # Check key parameters for a change of data
        update = False
        if ident in self.text_data:
            cmp_data = self.text_data[ident]
            for param in ["text", "color", "vertical_position", "horizontal_position"]:
                if data[param] != cmp_data[param]:
                    update = True
                    break
        else:
            update = True
        
        # Only do an update if the text has changed or update is set True (forced update scenario).
        if ident not in self.text_data or update:
            self.text_data[ident] = data
            self.update = True
        
    def text_error(self, text):
        """
        This is used to display error messages in the event of an issue.
        """        
        font = Font()
        font.LoadFont("fonts/6x10.bdf")
        self.canvas.Clear()
        DrawText(self.canvas, font, 0, 20, Color(255,50,50), text)
        self.matrix.SwapOnVSync(self.canvas)
        
    
#####
# Initialise the state and matrix, **in that order**.
# If they are done the other way around the I2C bus and martix HAT have a barny!
#####    

clock_state = ClockState.instance()
matrix = Matrix.instance()

####
# Initialise user signals.
####

def reload_settings(signum, frame) -> None:
    """
    Re-load the settings
    """    
    clock_settings.load_settings()
    # Force matrix update
    matrix.update = True
    print("Clock application: Settings reloaded.")

def poweroff(signum, frame) -> None:
    """
    Power the clock off.
    """
    clock_state.terminate = True
    matrix.text_error("Power off.")
    run(['/usr/bin/sudo','poweroff'])
    
def terminate(signum, frame) -> None:
    """
    Signal to all asyncs to terminate, which will terminate this application.
    """
    print("Clock application terminating.")
    matrix.text_error("Shut down.")
    clock_state.terminate = True


# Hook the signals on module load
signal.signal(signal.SIGUSR1, reload_settings)
signal.signal(signal.SIGUSR2, poweroff)
signal.signal(signal.SIGTERM, terminate)
signal.signal(signal.SIGINT, terminate)    
