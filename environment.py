'''
Created on 22 Jan 2021

Environment module providing environment service.

@author: philip
'''

import bme680
from asyncio import sleep as async_sleep
from rgbmatrix.graphics import Color
from utils import clock_settings, clock_state, matrix


class Environment:
    """
    Sample the environment.
    This has now been modified to use a Boch bme680 I2C sensor module.
    For the time being this does not do gas sampling.
    """
    # todo: extend this to include sensor readings from the GDS service.
    
    last_environment = None
    running = True
    paused = False
    switch = False
    
    async def service(self):
        """
        Environment service
        """
        self.color = Color(94,92,247)
        environment_sensor = clock_state.i2c_environment_sensor
        
        ##
        # Setup the sensor
        ##
        # Over-sampling - this is  a trade off between noise and accuracy. 
        environment_sensor.set_humidity_oversample(bme680.OS_2X)
        environment_sensor.set_pressure_oversample(bme680.OS_4X)
        environment_sensor.set_temperature_oversample(bme680.OS_8X)
        
        # Filter out transients, this is a trade off between response to changes Vs spurious changes caused by
        # short lived environment events (EG say shutting a door resulting in a momentary pressure change)
        environment_sensor.set_filter(bme680.FILTER_SIZE_3)
        
        switch = False
        
        print("Environment service running.")
        while not clock_state.terminate:
            if clock_state.environment_paused:
                # We have been paused by another service that wants our canvas real estate!
                await async_sleep(2)
                continue
            
            if environment_sensor.get_sensor_data():
                # Output data to the matrix switching between metric and imperial
                
                if switch:
                    # Metric + pressure
                    msg = f"{round((environment_sensor.data.temperature*1.8)+32,1)}F     {int(round(environment_sensor.data.pressure,0))}hPa"
                else:
                    # Imperial + humidity
                    msg = f"{round( environment_sensor.data.temperature,1)}C         {round(environment_sensor.data.humidity,1)}%"
                
                matrix.text(
                    text=msg,
                    font="fonts/Thintel.bdf",
                    color=clock_settings.environment_color,
                    vertical_position=32                    
                    )
                
                # Update the clock's environmental state.
                clock_state.temperature_clock = environment_sensor.data.temperature
                clock_state.pressure_clock = environment_sensor.data.pressure           
                clock_state.humidity_clock = environment_sensor.data.humidity
                
            switch = not switch
            await async_sleep(10)
            
        print("Environment service terminated.")
    