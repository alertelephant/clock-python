'''
Created on 14 Feb 2021
This is the main interface server run with a light weight fast WSGI compliant server called bjoern
This is a pure API server, it serves no HTML pages.
@author: philip
'''

import os, signal
from json import load, dump, loads, JSONDecodeError
from flask import Flask, jsonify, request
from pathlib import Path

app = Flask(__name__)

settings_path = Path("../settings.json")
state_path = Path("../state.json")

# Data post type mappings
type_mappings = {
    
    "news_always": bool,
    "environment_outside": bool,
    "marquee_scroll_delay": float,
    "marquee_scroll_distance": int,
    "dark_brightness": float,
    "lux_range": float
    }


@app.route("/settings/", methods=["GET"])
def get_settings():
    """
    Send back the settings to the requested server
    """
    
    data = None
    
    if settings_path.exists():
        with settings_path.open("r") as settings_file:
            try:
                data = load(settings_file) 
            except JSONDecodeError as ex:
                # Display error to console only.
                print(f"Clock: JSON decode error for settings file: {ex}")
    else:
        print("Clock: Settings path is missing, has the server been run up in it's own directory?")
        return jsonify({
                "error": "Missing settings file for clock."
            })
                
    return jsonify({
            "ok": True,
            "data": data    
        })

@app.route("/settings/update/", methods=["POST"])
def write_settings():
    """
    Upload updated settings as a URL encoded form entity
    """
    
    # We need the PID so we can send a sigusr1
    if state_path.exists():
        # Get the pid back
        pid = None
        with state_path.open("r") as state_file:
            pid = load(state_file).get("pid", None)
        
        if pid is None:
            return jsonify(
                {
                    "error": "Unable to get the clock PID!"
                }
            )
        
        # Load in the settings.
        with settings_path.open("r") as settings_file:
            settings = load(settings_file)
        
        # new_settings - data will be a JSON encoded object.
        new_settings = loads(request.data)
        
        # Update settings
        settings.update(new_settings)
        
        # Write back to the settings file
        with settings_path.open("w") as settings_file:
            dump(settings,settings_file)
        
        # Give the clock a kick, tell it to reload the settings!
        os.kill(pid, signal.SIGUSR1)
        
        # Return OK
        return jsonify(
                {
                    "ok": True
                }
            )
       
    
    return jsonify(
            {
                "error": "Unable to get the state of the clock, the clock has not generated a state.json file!"
            }
        )

@app.route("/state/", methods=["GET"])
def get_state():
    """
    Send back the clock state.
    """
    
    data = None
    
    if state_path.exists():
        with state_path.open("r") as settings_file:
            try:
                data = load(settings_file) 
            except JSONDecodeError as ex:
                # Display error to console only.
                print(f"Clock: JSON decode error for state file: {ex}")
    else:
        return jsonify({
                "error": "Missing state file for clock!"
            })
                
    return jsonify(
        {
            "ok": True,
            "data": data    
        })

if __name__ == "__main__":
    import bjoern

    bjoern.run(app, "0.0.0.0", 80, reuse_port=True)    
    