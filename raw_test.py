'''
Created on 13 Feb 2021
Test out raw access to matrix.
@author: philip
'''

from time import sleep
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from rgbmatrix.graphics import Font, DrawText, Color

matrix_options = RGBMatrixOptions()
matrix_options.rows = 32
matrix_options.cols = 64
matrix_options.chain_length = 1
matrix_options.parallel = 1
matrix_options.hardware_mapping = 'adafruit-hat-pwm'
matrix = RGBMatrix(options = matrix_options)
canvas = matrix.CreateFrameCanvas()

font = Font()
font.LoadFont("fonts/6x10.bdf")
color = Color(255,255,255)

DrawText(
    canvas,
    font,
    0, # hpos
    23, # vpos
    color,
    "TEST!"
    
    )
matrix.SwapOnVSync(canvas)
print("Swapped to display.")
sleep(20)