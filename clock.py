'''
Created on 13 Feb 2021
The main clock application, this is the runnable module.
@author: philip
'''
import os, pwd
from pytz import timezone
from time import sleep as sync_sleep
from asyncio import sleep as async_sleep, gather, run
from datetime import datetime
from environment import Environment
from utils import clock_settings, clock_state, matrix
from rgbmatrix.graphics import Font, Color
import traceback
from light import Light
from news import News

def get_username():
    return pwd.getpwuid( os.getuid() )[ 0 ]


class Clock:
    """
    The main clock application
    """
    
    def __init__(self, timezone_name="europe/london"):
        self.timezone = timezone(timezone_name)
    
    @property
    def time(self) -> str:
        """
        Returns the time formatted to Hour:minute:second
        """
        now = datetime.now(self.timezone)
        return now.strftime("%H:%M")
    
    @property
    def date(self) -> str:
        """
        Returns the abbreviated named date.
        """
        now = datetime.now(self.timezone)
        return now.strftime("%b %d %Y")
    
    async def run(self):
        """
        Setup and run the loops.
        """
        
        try:
            await gather(
                    matrix.service(),
                    self.service(),
                    Environment().service(),
                    Light().service(),
                    News().service(),
                    News().news_schedule_service(),
                    self.periodic_commit_state()
#                     self.test_marquee("Testing the marquee!")                    
                )
        except Exception as ex:
            clock_state.terminate=True
            print("Issue with clock: %s" % ex)
            traceback.print_exc()
            # Signal to the user that there has been an issue
            matrix.text_error("Error!")
            sync_sleep(10)
        
    
    async def service(self):
        """
        The clock service which provides the local time.
        """
        
        print("Clock service running.")
        while not clock_state.terminate:
            
            # Send to the matrix service the time
            matrix.text(
                text=self.time,
                font="fonts/VCR_OSD_MONO_1.001.bdf",
                color=clock_settings.time_color,
                vertical_position=23
                )
            
            await async_sleep(2)
        
        print("Clock service terminated.")
    
    async def periodic_commit_state(self):
        """
        Commit the state every 10 seconds.
        """
        print("Periodic state update service running.")
        while not clock_state.terminate:
            clock_state.commit_state()
            await async_sleep(10)
        
        print("Periodic state update service terminated.")
    
    async def test_marquee(self, text: str):
        """
        Marquee test service, displays the text passed in 'text' as a scrolling marquee, forever! 
        """

        while not clock_state.terminate:
            await async_sleep(1)
            matrix.text(
                    text=text,
                    font="fonts/Thintel.bdf",
                    color=clock_settings.environment_color,
                    marquee_direction=matrix.MarqueeDirection.RTL,
                    vertical_position=32
                )
            
        
if __name__ == "__main__":
    clock = Clock()
    run(clock.run())


