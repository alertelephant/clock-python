'''
Created on 14 Feb 2021
News ticker.
@author: philip
'''

# TODO: Based on the V1 clock, recreate the news ticker.
# This now uses scheduling to flip the news show flag in the state singleton.
from asyncio import sleep as async_sleep
from datetime import datetime
from requests.exceptions import ConnectionError, Timeout, RequestException
from requests import get
from utils import clock_settings, clock_state, matrix


class News:
    """
    News gathering and display.
    """
    
    news_url = "https://content.guardianapis.com/search"
    api_key = ""
        
    async def service(self):
        """
        Get and display the news when available, if requested to do so.
        """
        
        print("News service running.")
        news_showing = False
        # Store last text for re-load
        news_data = None
        while not clock_state.terminate:
            
            if not clock_settings.news_never:
                
                if (clock_state.news_active or clock_settings.news_always) and not news_showing:
                    
                    # gather up news, get_news will format the news for the marquee, including returning any errors.
                    news_data = await self.get_news()
                    
                    # Temporarily disable the environment, we want the matrix real estate!
                    clock_state.environment_paused=True
                    
                    # Send the news to the matrix
                    matrix.text(
                        text=news_data,
                        font="fonts/Thintel.bdf",
                        color=clock_settings.news_color,
                        vertical_position=32,
                        marquee_direction=matrix.MarqueeDirection.RTL,                  
                        )
                    # The marquee animation is handled by the matrix service, so we do not need to 
                    # repeatedly call marquee.text(...)
                    news_showing = True
                
                elif not (clock_state.news_active or clock_settings.news_always) and news_showing:
                    # Request to deactivate.
                    news_showing=False
                    clock_state.environment_paused=False
                
                if news_showing and clock_settings.news_reload:
                    # Force re-load of text.
                    matrix.text(
                        text=news_data,
                        font="fonts/Thintel.bdf",
                        color=clock_settings.news_color,
                        vertical_position=32,
                        marquee_direction=matrix.MarqueeDirection.RTL,                  
                        )
                    clock_settings.news_reload = False
            
            else:
                news_showing=False
                clock_state.environment_paused=False       
                        
            await async_sleep(5)
        
        print("News service terminated.")
    
    async def get_news(self):
        """
        Fetch the news from the Guardian API
        """
        
        try:
            resp = get(f"{self.news_url}?api-key={self.api_key}")
        except (ConnectionError, Timeout, RequestException) as ex:
            print("Communication or connection issue with the Guardian news API.\n"
                  f"Requests error: {ex}")
            return "Sorry no news, there was a communication issue with the Guardian new feed."
        
        if resp.status_code != 200:
            # We have had an issue
            print(
                "News service issue:\n"
                f"Non 200 return code from call to news service, code was {resp.status_code}\n"
                f"Raw output:\n{resp.content}"
                )
            return f"Sorry no news, general issue with Guardian news feed ({resp.status_code})."
        
        news_raw_data = resp.json()['response']
        
        if news_raw_data['status'] != "ok":
            # We have an issue
            print(
                "News service issue:\n"
                f"Status reported not 'ok'! Raw output:\n{repr(news_raw_data)}"
                )
            return f"Sorry no news, specific issue with Guardian news feed ({news_raw_data['status']})"
                
        # Process the news into marquee format.
        
        # Sort by section ids
        news_struct = {}
        
        for item in news_raw_data['results']:
            
            section_id = item['sectionId']
            
            if section_id in news_struct:
                news_struct[section_id].append(item['webTitle'])
            else:
                news_struct[section_id] = [item['webTitle']]
        
        # Convert to a single string
        news_marquee = ""
        
        for section, titles in news_struct.items():
            titles_string = "~".join(titles)
            news_marquee += f"::{section}:: {titles_string} "
        
        # We convert to all upper case since this displays better than mixed case, on a matrix display.
        return "-- GUARDIAN NEWS -- " + news_marquee.upper()
    
    def show_news(self):
        """
        Show the news for five minutes.
        """
        clock_state.news_active = True
        
    def hide_news(self):
        """
        Hide the news
        """
        clock_state.news_active = False
    
    async def news_schedule_service(self):
        """
        The news scheduler service.
        """
        
        print("News scheduler service running.")
        # There is no real scheduling system that works in an async environment.
        # So lets make one!
        while not clock_state.terminate:
            
            now = datetime.now().time()
            is_active = False
            # Scan the schedules and activate news if time falls inside any of them.
            # otherwise deactivate. 
            for start, end in clock_settings.news_time_segments:
                if start < now < end:
                    is_active = True
                    break
                
            clock_state.news_active = is_active
                        
            await async_sleep(30)
            
        print("News scheduler service terminated.")
