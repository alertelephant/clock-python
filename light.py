'''
Created on 14 Feb 2021

@author: philip
'''
from asyncio import sleep as async_sleep
from utils import clock_settings, clock_state, matrix

class Light:
    """
    Handles controlling the matrix brightness based on ambient LUX.
    """
    running = True
        
    async def service(self):
        light_sensor = clock_state.i2c_light_sensor
        last_brightness = None
        
        print("Light sensing service running.")
        while not clock_state.terminate:
            lux = light_sensor.lux
            
            # Update the LUX using the settings and our formula, we lock the brightness to 1
            brightness = min(1, round(clock_settings.dark_brightness + (lux/clock_settings.lux_range), 3))
            
            if brightness != last_brightness:
                last_brightness = brightness
                clock_settings.brightness = brightness
                # Force matrix update
                matrix.update=True
                
            await async_sleep(0.5)
        
        print("Light sensing service terminated.")